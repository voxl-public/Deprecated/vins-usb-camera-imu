/**
 *  @file utils.h
 *  @brief File containing utils needed by the main application
 */
/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef MODAL_AI_START_STOP_H
#define MODAL_AI_START_STOP_H

namespace Utils
{

// main() while loop should check this for shutdown
extern int main_running;

/**
 * @brief      Makes a PID file PID_FILE containing the current PID of your
 *             process
 *
 * @return     Returns 0 if successful. If that file already exists then it is
 *             not touched and this function returns 1 at which point we suggest
 *             you run kill_exising_process() to kill that process. Returns -1
 *             if there is some other problem writing to the file.
 */
int make_pid_file(const char* path);


/**
 * @brief      This function is used to make sure any existing program using the
 *             PID file is stopped.
 *
 *             The user doesn't need to integrate this in their own program
 *             However, the user may call the kill example program from the
 *             command line to close whatever program is running in the
 *             background.
 *
 * @param[in]  timeout_s  timeout period to wait for process to close cleanly,
 *                        must be >=0.1
 *
 * @return     return values:
 * - -4: invalid argument or other error
 * - -3: insufficient privileges to kill existing process
 * - -2: unreadable or invalid contents in PID_FILE
 * - -1: existing process failed to close cleanly and had to be killed
 * -  0: No existing process was running
 * -  1: An existing process was running but it shut down cleanly.
 */
int kill_existing_process(const char* path, float timeout_s);


/**
 * @brief      Removes the PID file created by make_pid_file().
 *
 *             This should be called before your program closes to make sure
 *             it's not left behind.
 *
 * @return     Returns 0 whether or not the file was actually there. Returns -1
 *             if there was a file system error.
 */
int remove_pid_file(const char* path);


/**
 * @brief      Enables a generic signal handler. Optional but recommended.
 *
 *             This catches SIGINT, SIGTERM, SIGHUP, and SIGSEGV and does the
 *             following:
 *
 * - SIGINT (ctrl-c) Sets process state to EXITING indicating to the user
 *   threads to shut down cleanly. All user threads should check get_state to
 *   catch this.
 * - SITERM: Same as SIGINT above
 * - SIGHUP: Ignored to prevent process from stopping due to loose USB network
 *   connection. Also allows robot control programs to keep running after USB
 *   cable in intentionally removed.
 * - SIGSEGV:  Segfaults will be caught and print some debugging info to the
 *   screen before setting state to EXITING. Behavior with segfaults is no
 *   guaranteed to be predictable.
 *
 * @return     Returns 0 on success or -1 on error
 */
int enable_signal_handler(void);


}

#endif // START_STOP_H

