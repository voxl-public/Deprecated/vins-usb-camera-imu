/**
 *  @file VinsCameraImu.h
 *  @brief Driver Class for the Modal AI M0018 VINS Camera-IMU Board
 */
/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef MODAL_AI_VINS_CAMERA_IMU_h
#define MODAL_AI_VINS_CAMERA_IMU_h

// 3rd Party Includes
#include "libuvc/libuvc.h"

#ifdef __ANDROID__
#include <libusb.h>
#else
#include <libusb-1.0/libusb.h>
#endif

// C++ Includes
#include <limits>
#include <thread>
#include <atomic>

class VinsCameraImu
{
private:
	/** general Constants
	 */
	static constexpr uint16_t BOARD_VENDOR_ID = 0x0483;
	static constexpr uint16_t BOARD_PRODUCT_ID = 0x00F1;

public:
	/** Camera Constants
	 */
	static constexpr uint16_t CAMERA_WIDTH_PIXELS = 640;
	static constexpr uint16_t CAMERA_HEIGHT_PIXELS = 480;
	static constexpr uint16_t CAMERA_FPS = 30;
	static constexpr uint16_t CAMERA_GAIN_MIN_VALUE = 0;
	static constexpr uint16_t CAMERA_GAIN_MAX_VALUE = 1023;
	static constexpr uint32_t CAMERA_EXPOSURE_MIN_USEC = 100;
	static constexpr uint32_t CAMERA_EXPOSURE_MAX_USEC = 33000;

	/** IMU Constants
	 */
	static constexpr uint16_t IMU_TRANSFER_NUMBER_OF_PACKETS = 1;
	static constexpr uint16_t IMU_TRANSFER_DEV_PACKET_LENGTH = 64;
	static constexpr uint8_t IMU_DEVICE_ENDPOINT = 0x84;
	static constexpr uint8_t IMU_DEVICE_INTERFACE = 2;
	static constexpr double IMU_ACCEL_LSB_PER_G = 2048.0;
	static constexpr double IMU_GYRO_LSB_PER_DEG_PER_SEC = 16.4;
	static constexpr uint16_t IMU_MAX_FREQUENCY = 500;

	/** The IMU Data
	 */
	struct ImuData
	{
		/** The accelerometer data.
		 *  [0] -> X axis
		 *  [1] -> Y axis
		 *  [2] -> Z axis
		 */
		double accel_values[3];

		/** The gyro data.
		 *  [0] -> X axis
		 *  [1] -> Y axis
		 *  [2] -> Z axis
		 */
		double gyro_values[3];

		/** The timestamp the samples were taken at
		 */
		uint64_t timestamp;
	} __attribute__((packed));

	/** The Camera Data
	 */
	struct CameraData
	{
		/** The raw pixels from the camera.
		 */
		uint8_t camera_pixels[CAMERA_WIDTH_PIXELS * CAMERA_HEIGHT_PIXELS];

		/** The timestamp the camera frame were taken at.
		 */
		uint64_t timestamp;
	} __attribute__((packed));



	/** The callback functions types that the user should use to get data
	 */
	typedef void (*CameraFrameReadyCallback)(CameraData &data);
	typedef void (*ImuDataReadyCallback)(ImuData &data);

	/** Constructor
	 */
	VinsCameraImu();

	/** Destructor
	 */
	~VinsCameraImu();

	/** Start the camera stream
	 *
	 *  @throws std::runtime_error if the camera cannot be started
	 */
	void startCameraStream();

	/** Stop the camera stream
	 *
	 *  @throws std::runtime_error if the camera cannot be stopped
	 */
	void stopCameraStream();

	/** Set the camera exposure
	 *
	 *  @param[in] new_exposure the new exposure for the camera in microseconds
	 *  @throws std::runtime_error if the camera exposure cannot be set
	 */
	void setCameraExposure(uint32_t new_exposure);

	/** Get the camera exposure
	 *
	 *  @return The current exposure for the camera in microseconds
	 *  @throws std::runtime_error if the camera exposure cannot be retrieved
	 */
	uint32_t getCameraExposure();

	/** Set the camera gain
	 *
	 *  @param[in] new_gain The new gain for the camera
	 *  @throws std::runtime_error if the camera gain cannot be set
	 */
	void setCameraGain(uint16_t new_gain);

	/** Get the camera gain
	 *
	 *  @return The current gain for the camera
	 *  @throws std::runtime_error if the camera gain cannot be retrieved
	 */
	uint16_t getCameraGain();

	/** Start the IMU streaming data
	 *
	 *  @param[in] frequency The frequency to generate data at
	 *  @throws std::runtime_error if the imu data stream cannot be started
	 */
	void startImuDataStream(uint16_t frequency);

	/** Stop the IMU data stream
	 *
		@throws std::runtime_error if the IMU was not initialized
	 */
	void stopImuDataStream();

	/** Register the callback to use for when camera data is ready
	 *
	 *  @param[in] callback The callback function to use
	 */
	void registerCameraFrameCallback(CameraFrameReadyCallback callback);

	/** Register the callback to use for when IMU data is ready
	 *
	 *  @param[in] callback The callback function to use
	 */
	void registerImuDataCallback(ImuDataReadyCallback callback);

	/** Set the Gyro Zero offset.
	 *  This should be called before startImuDataStream(...) is called.
	 *
	 *  @param[in] offsets the zero offsets for the gyro
	 *  @throws std::runtime_error If the IMU is currently streaming
	 */
	void setGyroZeroOffset(const double offsets[3]);

private:
	/** The Raw IMU Data
	 */
	struct ImuDataRaw
	{
		/** The accelerometer data.
		 *  [0] -> X axis
		 *  [1] -> Y axis
		 *  [2] -> Z axis
		 */
		int16_t accel_raw[3];

		/** The gyro data.
		 *  [0] -> X axis
		 *  [1] -> Y axis
		 *  [2] -> Z axis
		 */
		int16_t gyro_raw[3];

		/** The timestamp the samples were taken at
		 */
		uint64_t timestamp_us;
	} __attribute__((packed));


	/** Init the camera device
	 *
	 *  @throws runtime_error If the camera could not be initialized
	 */
	void initCamera();

	/** Init the IMU
	 *
	 *  @throws runtime_error If the imu could not be initialized
	 */
	void initImu();

	/** Convert the IMU data into the scaled and unbiased outputs
	 *  The following is done:
	 *  	- Scales the IMU data by the correct scaling factor
	 *		- Remove Gyro Zero offset
	 */
	void convertImuData(const ImuDataRaw& raw, ImuData &converted);

	/** A static helper function that is a jumper to call "cameraFrameReadyInternalCallback(...)"
	 *  This is needed because of the way that the UVC driver does callbacks
	 *
	 *  @param[in] frame The latest camera frame from the UVC driver
	 *  @param[in] ptr the pointer to this class
	 */
	static void cameraFrameReadyInternalCallbackHelper(uvc_frame_t *frame, void *ptr);

	/** The callback that processes a new camera frame
	 *
	 *  @param[in] frame the new camera frame
	 */
	void cameraFrameReadyInternalCallback(uvc_frame_t *frame);

	/** A static helper function that is a jumper to call "imuDataReadyInternalCallback(...)"
	 *  This is needed because of the way that the libusb driver does callbacks
	 *
	 *  @param[in] transfer the USB transfer that had an event
	 */
	static void imuDataReadyInternalCallbackHelper(struct libusb_transfer *transfer);

	/** The callback that processes a USB transfer (which is usually new IMU data)
	 *
	 *  @param[in] transfer The USB transfer
	 */
	void imuDataReadyInternalCallback(struct libusb_transfer *transfer);

	/** The IMU data worker that runs on a separate thread
	 */
	void imuWorkerFunction();

	/** The camera UVC context used for interacting with the UVC driver
	 */
	uvc_context_t *camera_uvc_context{nullptr};

	/** The camera device
	 */
	uvc_device_t *camera_device{nullptr};

	/** The device handle for the camera
	 */
	uvc_device_handle_t *camera_device_handle{nullptr};

	/** The Camera Control struct
	 */
	uvc_stream_ctrl_t camera_control;

	/** If the camera is streaming or not
	 */
	bool camera_is_streaming{false};

	/** The USB context
	 */
	libusb_context * 	usb_context;

	/** The USB device list
	 */
	libusb_device **usb_device_list;

	/** The IMU device handle
	 */
	libusb_device_handle *imu_device_handle{nullptr};

	/** The IMU device USB transfer
	 */
	libusb_transfer *imu_device_transfer{nullptr};

	/** The transfer buffer to use for the IMU transfers
	 */
	unsigned char imu_transfer_buffer[IMU_TRANSFER_DEV_PACKET_LENGTH * IMU_TRANSFER_NUMBER_OF_PACKETS];


	/** If the IMU was initialized
	 */
	bool imu_initted{false};

	/** The camera frame ready callback that the user registered
	 */
	CameraFrameReadyCallback camera_frame_ready_callback{nullptr};

	/** The camera frame ready callback that the user registered
	 */
	ImuDataReadyCallback imu_data_ready_callback{nullptr};

	/** Set the last set exposure and gain
	 */
	uint32_t last_set_exposure = std::numeric_limits<uint32_t>::max();
	uint16_t last_set_gain = std::numeric_limits<uint16_t>::max();

	/** The IMU thread
	 */
	std::thread imu_thread;

	/** If the IMU should keep running
	 */
	std::atomic<bool> imu_thread_keep_running{false};

	/** The period to use in the imu thread loop
	 */
	uint32_t imu_loop_period{0};

	/** The gyro offsets
	 */
	double gyro_zero_offset[3] = {0, 0, 0};
};

#endif
