#!/bin/bash

# Build
mkdir -p build
cd build
cmake ../
make -j4
cd ../